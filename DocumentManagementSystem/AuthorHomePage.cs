﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DocumentManagementSystem
{
    public partial class AuthorHomePage : Form
    {
        public AuthorHomePage(string username)
        {
            InitializeComponent();
            lblAuthorHomePage.Text = username;
        }

        private void lblDocumentManagement_Click(object sender, EventArgs e)
        {

        }

        private void btnBackToUser_Click(object sender, EventArgs e)
        {

        }

        private void lblYourDocuments_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            // should be btnUploadNewRev

        }

        private void btnCreateDocRec_Click(object sender, EventArgs e)
        {
            string Username = lblAuthorHomePage.Text;
            //AuthorHomePage x = new AuthorHomePage();
            CreateDocument frmCreateDocument = new CreateDocument(Username);
            frmCreateDocument.Show();
        }
    }
}
