﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentManagementSystem
{
    public partial class frmLoginScreen : Form
    {
        public frmLoginScreen()
        {
            InitializeComponent();
        }

        private void btnSubmitLogin_Click(object sender, EventArgs e)
        {
            //Assign variables from user entered into text boxes
            string username = txtUsername.Text;
            string password = txtPassword.Text;

            User currentUser = new User(username, password);

            if (!currentUser.isPassword)
            {
                /*
                 * WrongPasswordForm wrong = new WrongPasswordForm();
                 * wrong.ShowDialog();
                 */
            }
            else
            {
                /* NickConnell
                 * Open various pages/panels depending on role
                 * Roles:
                 * 0 = Author
                 * 1 = Distributee
                 * 2 = Administrator
                 * 
                 */

                // declare variables
                string authorRole = "Author";
                string distibuteeRole = "Distributee";
                string adminRole = "Administrator";

                //if statements to check for roles to open correct screens
                if (currentUser.role == authorRole)
                {
                    AuthorHomePage frmAuthor = new AuthorHomePage(username);
                    frmAuthor.Show();
                }
                else if (currentUser.role == distibuteeRole)
                {
                    Distributee frmDistributee = new Distributee();
                    frmDistributee.Show();
                }
                else if (currentUser.role == adminRole)
                {
                    Administrator frmAdministrator = new Administrator();
                    frmAdministrator.Show();
                }
            }

            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
        }

        private void frmLoginScreen_Load(object sender, EventArgs e)
        {

        }
    }
}
